package test.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import test.client.FileService;
import test.client.MyFile;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import cz.osjd.classfile.ClassFile;


public class FileServiceImpl extends RemoteServiceServlet implements
		FileService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7615776998783207409L;
	public static final String FOLDER_URL = "http://www.stud.fit.vutbr.cz/~xmacha48/list.txt";
	public static final String MY_URL = "http://www.stud.fit.vutbr.cz/~xmacha48/";
	public static final String LIST = "/list.txt";

	/**
	 * Get all class files in folder name recursively.
	 */
	public List<MyFile> getFiles(String name) throws IllegalArgumentException {
		/**
		 * This is really bad, should be independet,
		 * not dependet only on files. (other options GDrive, dorpbox,..)
		 */
		URL url;
		String line;
		List<MyFile> filesNames = new ArrayList<MyFile>();;
		
		System.out.println("Found files:");
		try {
			for(String folder : getFolders()) {
				url = new URL(MY_URL + folder + LIST);
				URLConnection connection = url.openConnection();
				BufferedReader input = new BufferedReader( new InputStreamReader( connection.getInputStream() ));
				
				while( (line = input.readLine()) != null) {
					System.out.println("\t" + line);
					filesNames.add(new MyFile(line, folder));
				}
			}
		} catch (Exception e) {
			System.out.println("Error while reading file: " + e.toString());
			return new ArrayList<MyFile>();
		}
		
		return filesNames;
	}

	/**
	 * Read file name. Absoule path to file.
	 */
	public String readFile(String name) throws IllegalArgumentException {
		// TODO: throws IOException....
		System.out.println("Reding file: " + name);
		try {
			URL url = new URL(MY_URL + name);
			ClassFile cf = new ClassFile(url);
			cf.read();
			return StringEscapeUtils.escapeHtml(cf.toStringText()).replaceAll("\n", "<br/>").replaceAll(" ", "&nbsp;");
		} catch (FileNotFoundException e) {
			return "File not found";
		} catch (Exception e) {
			return e.toString();
		}
	}

	public List<String> getFolders() throws IllegalArgumentException {
		return getFolders(FOLDER_URL);
	}
	
	/**
	 * Get list of folders at specified url
	 */
	public List<String> getFolders(String urlLoc) throws IllegalArgumentException {
		if(urlLoc == null) {
			urlLoc = FOLDER_URL;
		}
		
		List<String> folders = new ArrayList<String>();
		String line;
		try {
			URL url = new URL(urlLoc);
			URLConnection connection = url.openConnection();
			
			BufferedReader input = new BufferedReader( new InputStreamReader( connection.getInputStream() ));
			
			while( (line = input.readLine()) != null) {
				folders.add(line);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Failed on fetching folders.");
		}
		return folders;
	}

}