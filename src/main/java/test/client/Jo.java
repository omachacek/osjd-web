package test.client;

import java.util.ArrayList;
import java.util.List;

import test.client.MainArea;
import test.client.NavigationArea;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootLayoutPanel;


import com.smartgwt.client.util.Page;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Jo implements EntryPoint {

	private final FileServiceAsync fileService = GWT
			.create(FileService.class);
	private final DisassembleServiceAsync disassembleService = GWT
			.create(DisassembleService.class);
	
	private static final int HEADER_HEIGHT = 85;
	
	private VLayout mainLayout;
	private HLayout northLayout;
	private HLayout southLayout;
	private VLayout eastLayout;
	private HLayout westLayout;
	
	private List<MyFile> files;
	private List<String> folders;
	
	public void onModuleLoad() {
		Page.setAppImgDir("[APP]/Osjd/images/");
		
		AsyncCallback<List<MyFile>> callback = new AsyncCallback<List<MyFile>>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}
			
			public void onSuccess(List<MyFile> result) {
				files = new ArrayList<MyFile>();
				for(MyFile s : result) { 
					files.add(s);
				}
				create();
			}
		};
		fileService.getFiles(null, callback);
		
		AsyncCallback<List<String>> callback2 = new AsyncCallback<List<String>>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}
			
			public void onSuccess(List<String> result) {
				folders = new ArrayList<String>();
				for(String s : result) { 
					folders.add(s);
				}
				create();
			}
		};
		fileService.getFolders(null, callback2);
		create();
	}
	
	public void create() {
		Window.enableScrolling(false);
		Window.setMargin("0px");
		
		mainLayout = new VLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight100();

		northLayout = new HLayout();
		northLayout.setHeight(HEADER_HEIGHT);

		VLayout vLayout = new VLayout();
		vLayout.addMember(new HeaderArea());
		northLayout.addMember(vLayout);

		eastLayout = new MainArea(disassembleService);
		eastLayout.setWidth("85%");
		
		westLayout = new NavigationArea(folders, files, eastLayout, fileService);
		westLayout.setWidth("15%");
		
		southLayout = new HLayout();
		southLayout.setMembers(westLayout, eastLayout);

		mainLayout.addMember(northLayout);
		mainLayout.addMember(southLayout);

		RootLayoutPanel.get().add(mainLayout);
	}
  
}
