
package test.client;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class MainArea extends VLayout {

	final TabSet topTabSet = new TabSet();
	final DisassembleServiceAsync disService;
	
	public MainArea(final DisassembleServiceAsync disassembleServiceAsync) {
		super();
		this.disService = disassembleServiceAsync;
		this.setOverflow(Overflow.HIDDEN);
		
		topTabSet.setTabBarPosition(Side.TOP);  
		topTabSet.setTabBarAlign(Side.LEFT);
		
		this.addMember(topTabSet);
		
	}
	
	public void addTab(final String title, String text, boolean disassembled) {
		ToolStrip toolStrip = new ToolStrip();
		toolStrip.setWidth100();
		ToolStripButton disassemble = new ToolStripButton();
		disassemble.setDisabled(disassembled);
		disassemble.setEdgeOpacity(33);
		disassemble.setEdgeSize(11);
		disassemble.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
					}
					
					public void onSuccess(String result) {
						addTab("Disassembled", result, true);
					}
				};
				disService.disassemble(title, callback);
			}
		});
		disassemble.setTitle("Disassemble");
		toolStrip.addButton(disassemble);
		
		VLayout hlayout = new VLayout();
		hlayout.addMember(toolStrip);
		
		final Canvas htmlCanvas = new Canvas();  
        htmlCanvas.setPadding(2);  
        htmlCanvas.setOverflow(Overflow.AUTO);    
        htmlCanvas.setShowEdges(true); 
        htmlCanvas.setContents(text);  
		
		hlayout.addMember(htmlCanvas);
		
		Tab tab = createTab(title, hlayout, true);
		
		topTabSet.addTab(tab);
		topTabSet.selectTab(tab);
	}
	
	private Tab createTab(String title, Canvas pane, boolean closable) {
		Tab tab = new Tab(title);
		tab.setCanClose(closable);
		tab.setPane(pane);
		return tab;
	}
	
}

