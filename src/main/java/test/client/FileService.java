package test.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("file")
public interface FileService extends RemoteService {
	List<MyFile> getFiles(String name) throws IllegalArgumentException;
	List<String> getFolders(String name) throws IllegalArgumentException;

	String readFile(String name) throws IllegalArgumentException;
}