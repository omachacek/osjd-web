package test.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Overflow;  
import com.smartgwt.client.types.VisibilityMode;  
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;  
import com.smartgwt.client.widgets.ImgButton;  
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.form.DynamicForm;  
import com.smartgwt.client.widgets.form.fields.SelectItem;  
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;  
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;  
import com.smartgwt.client.widgets.grid.ListGrid;  
import com.smartgwt.client.widgets.grid.ListGridField;  
import com.smartgwt.client.widgets.grid.ListGridRecord;

import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;  
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;

public class NavigationArea extends HLayout {
	
	private List<MyFile> classFiles;
	private List<String> folders;
	
	public NavigationArea(final List<String> folders, final List<MyFile> files, final VLayout mainArea, final FileServiceAsync fileService) {
		this.classFiles = files;
		this.folders = folders;
		
		final ListGrid mylistGrid = new ListGrid();  
        mylistGrid.setFields(new ListGridField("name", "Name")); // add size ?
		
		final ListGrid listGrid = new ListGrid();  
        listGrid.setFields(new ListGridField("name", "Name")); // add size ?
        listGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				final ClassFileRecord record = ((ClassFileRecord)event.getRecord());
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
					}
					public void onSuccess(String result) {
						((MainArea)mainArea).addTab(record.getParent() + "/" + record.getName(), result, false);
					}
				};
				fileService.readFile(record.getParent() + "/" + record.getName(), callback);
			}
		});
  
        ImgButton addButton = new ImgButton();  
        addButton.setSrc("[SKIN]actions/add.png");
        addButton.setSize(16);  
        addButton.setShowFocused(false);  
        addButton.setShowRollOver(false);  
        addButton.setShowDown(false);  
        addButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
            	SC.say("Sorry, but uploading own class files, is not yet supported. Please come later.");
			}  
        });  
  
        DynamicForm form = new DynamicForm();  
        form.setHeight(1);  
        //form.setWidth(75);  
        form.setNumCols(1);  
  
        SelectItem selectItem = new SelectItem();  
        //selectItem.setWidth(120);  
        selectItem.setShowTitle(false);
        if(this.folders != null)
        selectItem.setValueMap(this.folders.toArray(new String[this.folders.size()]));  

        selectItem.setDefaultToFirstOption(true);
        listGrid.setData(ClassFileData.getRecords(classFiles, "stack"));
        selectItem.addChangeHandler(new ChangeHandler() {
			  
            public void onChange(ChangeEvent event) {  
            	System.out.println("Switching to " + (String)event.getValue());
            	listGrid.setData(ClassFileData.getRecords(classFiles, (String)event.getValue()));
            }
         });  
  
        form.setFields(selectItem);
  
  
        SectionStack sectionStack = new SectionStack();  
  
        SectionStackSection section1 = new SectionStackSection();  
        section1.setTitle("My class files");  
        section1.setItems(mylistGrid);
        section1.setControls(addButton);  
        section1.setExpanded(true);  
  
        SectionStackSection section2 = new SectionStackSection();  
        section2.setTitle("Examples");  
        section2.setItems(listGrid);
        section2.setControls(form);  
        section2.setExpanded(true);  
  
        sectionStack.setSections(section1, section2);  
        sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);  
        sectionStack.setAnimateSections(true);  
        //sectionStack.setWidth(300);  
        //sectionStack.setHeight(400);  
        sectionStack.setOverflow(Overflow.HIDDEN);  
  
        this.addMember(sectionStack);
    }
  
    class StatusCanvas extends Canvas {  
        StatusCanvas() {  
            setPadding(5);  
  
        }  
  
        public void setNewStatus(String status) {  
            setContents(status + ": <span style='color:green;font-weight:bold'>Normal</span><br>");  
        }  
    }  
	
}

class ClassFileRecord extends ListGridRecord {
	
	private String parent;
	
	public ClassFileRecord(String name, String parent) {
		setName(name);
		this.parent = parent;
	}
	
	public String getName() {
		return getAttributeAsString("name");
	}
	
	public void setName(String name) {
		setAttribute("name", name);
	}
	
	
	public String getParent() {
		return parent;
	}
	
	public void setParent(String parent) {
		this.parent = parent;
	}
}

class ClassFileData {

	  private static ClassFileRecord[] records;

	  public static ClassFileRecord[] getRecords(List<MyFile> files, String type) {
	    //if (records == null) {
	      records = getNewRecords(files, type);
	    //}
	    return records;
	  }

	  public static ClassFileRecord[] getNewRecords(List<MyFile> files, String type) {
		  if(files == null)
			  return new ClassFileRecord[]{};
		  System.out.println(type);
		  List<ClassFileRecord> r = new ArrayList<ClassFileRecord>();
		  for(MyFile f : files) {
			  if(f.getParent().equals(type)) {
				  r.add(new ClassFileRecord(f.getFileName(), f.getParent()));
			  }
		  }
		  return r.toArray(new ClassFileRecord[r.size()]);
	  }
}
