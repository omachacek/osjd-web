package test.client;

import com.google.gwt.user.client.ui.HTML;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;

public class HeaderArea extends HLayout {

	private static final int HEADER_AREA_HEIGHT = 65;

	public HeaderArea() {

		super();

		this.setHeight(HEADER_AREA_HEIGHT);
		/*
		final Img img = new Img();
        img.setWidth(48);
        img.setHeight(48);
        img.setParentElement(this);
        //img.setSrc("Osjd/images/java_logo.jpg");
        //img.draw();
		
		HLayout logoLayout = new HLayout();
		logoLayout.setAlign(Alignment.LEFT);
		logoLayout.setWidth("20%");
		logoLayout.addMember(img);
		*/
		Label name = new Label();
        name.setOverflow(Overflow.HIDDEN);  
        name.setContents("<center><b><h2>Open source java disassembler</h2></b></center>");  
	    
	    HLayout westLayout = new HLayout();
	    westLayout.setHeight(HEADER_AREA_HEIGHT);	
	    westLayout.setWidth("60%");
	    //westLayout.addMember(logo);
	    westLayout.addMember(name);

	    HLayout eastLayout = new HLayout();
	    eastLayout.setAlign(Alignment.RIGHT);  
	    eastLayout.setHeight(HEADER_AREA_HEIGHT);
	    eastLayout.setWidth("20%");
	    
	    //this.addMember(logoLayout);
		this.addMember(westLayout);  	
		this.addMember(eastLayout);

	}
}
