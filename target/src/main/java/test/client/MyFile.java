package test.client;

public class MyFile implements java.io.Serializable {

	private String fileName;
	private String parent;
	
	public MyFile() {
		this.fileName = null;
		this.parent = null;
	}
	
	public MyFile(String filename, String parent) {
		this.fileName = filename;
		this.parent = parent;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getParent() {
		return parent;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public void setParent(String parent) {
		this.parent = parent;
	}
	
}
