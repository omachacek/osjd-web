package test.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;

import org.apache.commons.lang.StringEscapeUtils;

import test.client.DisassembleService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;

import cz.osjd.classfile.ClassFile;
import cz.osjd.classfile.printer.ClassFilePrinter;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DisassembleServiceImpl extends RemoteServiceServlet implements
		DisassembleService {

	public String disassemble(String name) throws IllegalArgumentException {
		ClassFile cf = null;
		try {
			URL url = new URL(FileServiceImpl.MY_URL + name);
			cf = new ClassFile(url);
			cf.read();
		} catch(Exception e) {
			return e.toString();
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		
		String out = "";
		try {
			out = XhtmlRendererFactory.getRenderer("java").highlight(name, cf.toStringJavaCode(), "utf-8", false);
		} catch (IOException e) {
			
		}
		
		return out;
		//return StringEscapeUtils.escapeHtml(out).replaceAll("\n", "<br/>").replaceAll(" ", "&nbsp;");
	}
}