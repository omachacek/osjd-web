package test.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MyFile_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFileName(test.client.MyFile instance) /*-{
    return instance.@test.client.MyFile::fileName;
  }-*/;
  
  private static native void setFileName(test.client.MyFile instance, java.lang.String value) 
  /*-{
    instance.@test.client.MyFile::fileName = value;
  }-*/;
  
  private static native java.lang.String getParent(test.client.MyFile instance) /*-{
    return instance.@test.client.MyFile::parent;
  }-*/;
  
  private static native void setParent(test.client.MyFile instance, java.lang.String value) 
  /*-{
    instance.@test.client.MyFile::parent = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, test.client.MyFile instance) throws SerializationException {
    setFileName(instance, streamReader.readString());
    setParent(instance, streamReader.readString());
    
  }
  
  public static test.client.MyFile instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new test.client.MyFile();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, test.client.MyFile instance) throws SerializationException {
    streamWriter.writeString(getFileName(instance));
    streamWriter.writeString(getParent(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return test.client.MyFile_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    test.client.MyFile_FieldSerializer.deserialize(reader, (test.client.MyFile)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    test.client.MyFile_FieldSerializer.serialize(writer, (test.client.MyFile)object);
  }
  
}
