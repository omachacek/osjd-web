package test.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MyFile_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, test.client.MyFile[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static test.client.MyFile[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new test.client.MyFile[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, test.client.MyFile[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return test.client.MyFile_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    test.client.MyFile_Array_Rank_1_FieldSerializer.deserialize(reader, (test.client.MyFile[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    test.client.MyFile_Array_Rank_1_FieldSerializer.serialize(writer, (test.client.MyFile[])object);
  }
  
}
