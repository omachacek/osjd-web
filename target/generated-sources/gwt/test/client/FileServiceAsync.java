package test.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public interface FileServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see test.client.FileService
     */
    void getFiles( java.lang.String name, AsyncCallback<java.util.List<test.client.MyFile>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see test.client.FileService
     */
    void getFolders( java.lang.String name, AsyncCallback<java.util.List<java.lang.String>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see test.client.FileService
     */
    void readFile( java.lang.String name, AsyncCallback<java.lang.String> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static FileServiceAsync instance;

        public static final FileServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (FileServiceAsync) GWT.create( FileService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
