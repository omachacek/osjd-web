package test.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public interface DisassembleServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see test.client.DisassembleService
     */
    void disassemble( java.lang.String name, AsyncCallback<java.lang.String> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static DisassembleServiceAsync instance;

        public static final DisassembleServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (DisassembleServiceAsync) GWT.create( DisassembleService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
